package com.example.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.model.Food;

public interface FoodRepository extends JpaRepository<Food, Long> {

	@Query("SELECT obj FROM Food obj WHERE obj.id = :id")
	Optional<Food> findFoodById(Long id);
}
