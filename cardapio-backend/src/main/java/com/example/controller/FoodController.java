package com.example.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.dto.FoodRequestDTO;
import com.example.dto.FoodResponseDTO;
import com.example.service.FoodService;

@RestController
@RequestMapping("/api/v1/food")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FoodController {

	private final FoodService foodService;

	public FoodController(FoodService foodService) {
		this.foodService = foodService;
	}

	@GetMapping
	public ResponseEntity<List<FoodResponseDTO>> getAll() {
		List<FoodResponseDTO> foodList = foodService.findAll();
		return ResponseEntity.ok(foodList);
	}

	@PostMapping
	ResponseEntity<FoodResponseDTO> insert(@RequestBody FoodRequestDTO data) {
		FoodResponseDTO dto = foodService.insert(data);
		return ResponseEntity.status(201).body(dto);
	}

	@GetMapping("/{id}")
	ResponseEntity<FoodResponseDTO> findFoodById(@PathVariable("id") Long id) {
		FoodResponseDTO dto = foodService.findFoodById(id);
		return ResponseEntity.status(200).body(dto);
	}
}
