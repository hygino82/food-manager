package com.example.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.dto.FoodRequestDTO;
import com.example.dto.FoodResponseDTO;
import com.example.model.Food;
import com.example.repository.FoodRepository;

@Service
public class FoodService {

	@Autowired
	private FoodRepository foodRepository;

	@Transactional(readOnly = true)
	public List<FoodResponseDTO> findAll() {
		var list = foodRepository.findAll().stream().map(FoodResponseDTO::new).collect(Collectors.toList());
		return list;
	}

	@Transactional
	public FoodResponseDTO insert(FoodRequestDTO data) {
		Food entity = new Food(data);
		entity = foodRepository.save(entity);

		return new FoodResponseDTO(entity);
	}

	public FoodResponseDTO findFoodById(Long id) {
		Food entity = foodRepository.findFoodById(id).orElseThrow(() -> new IllegalArgumentException("Food not found"));

		return new FoodResponseDTO(entity);
	}
}
