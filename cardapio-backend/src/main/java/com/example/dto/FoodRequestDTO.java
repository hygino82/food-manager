package com.example.dto;

public record FoodRequestDTO(String title, String image, Integer price) {

}
