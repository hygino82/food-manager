export interface FoodData {
    price: number,
    title: string,
    id?: number,
    image: string
}